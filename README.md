# SADE: Services as DOM Elements #

This repo contains a tiger tail style landing page which showcases a pattern I think is pretty interesting -- **representing functionality on pages with HTML/DOM elements**.

I walk through [the motivation and development process in a related blog post](https://vadosware.io/post/sade-pattern-services-as-dom-elements/).

The idea is simple -- in a world where native webcomponents are the norm, what if we could make data-sharing and binding easier by building components to represent our data dependencies and the objects that mange them? If we can standardize the interconnect and contracts, maybe we don't need to wait until data binding works across frameworks (many frameworks compile to native web components, but leave you with no way to share data).

## An example snippet

Here's an short example of what the idea might look like, when used well on a minimal HTML page:

```html
<!-- SADE compliant "data elements" -->
<sade>
  <window-scroll-service id="scroll-svc"></window-scroll-service>

  <user-service id="user-svc">
    <initial-data>
      <user first-name="jane" last-name="doe" email="jane.doe@example.com"/>
    </initial-data>
  </user-service>

  <mailing-list-service id="mailing-list-svc"></mailing-list-service>
</sade>

<!-- Your UI components -->
<nav-bar brand="SADE" scroll-svc="#scroll-svc" reappear-on-scroll-up> ... </nav-bar>

<jumbotron-stripe bg-image-src="static/images/space-bg.jpg"> ... </jumbotron-stripe>

<email-collection-stripe
    cta-button-text="Get email updates"
    mailing-list-svc="#mailing-list-svc"> ... </email-collection-stripe>
```

With sufficiently robust services, there would be no reason to write a single line of JS to make things work for the most basic pages.

# Getting started

## Building the code

0. Clone this repo
1. `make`
1. `make build`

(if this set of instructions does not work for you)

## Running the code

To run the example landing page that ties everything together, run:

```console
$ make serve
```

# Services ("Data elements")

The focus of the SADE concept is data services/stores being exposed as DOM elements. To create a SADE-compliant service/store or "data element" you need to:

- Write one or more `interface`s that encapsulates service functionality
- Represent that functionality with one or more URIs
- Write a plan object/class (ex. `YourService`) that implements said interface(s)
- Make that object an `HTMLElement` (usually by wrapping and/or proxying it)

## `<window-scroll-service>`

The `WindowScrollService` does what you'd expect, it makes window scrolling behavior/events accessible to UI components.

## `<user-service>`

The `UserService` does things you might expect to do on a landing page for a new service, like creating new user accounts (and triggering registration emails to be sent on a backend).

## `<mailing-list-service>`

The `MailingListService` does things you might expect to do on landing page for a new service, like registering new members to a mailing list.

## `<kv-service>`

The `KeyValueService` does what you'd expect -- stores and retrieves keys and values in a simple object (not even a `Map`).

# UI Components

At the end of the day, we of course need some UI components to show the actual page, so there are some in this repo as well. The UI components are actually written in a variety of frontend frameworks, so this repo might inadvertently serve as a not-terrible introduction to how to use these libraries (with [Parcel](https://parceljs.org/)) to produce components.

## `<navbar>` ([Lit-Element](https://lit-element.polymer-project.org/))

A top navigation bar with a sparse set of links and branding that disappears when you scroll down (by making use of a `<scroll-service>`) and re-appears when you scroll up.

## `<jumbotron>` ([Slim.JS](https://github.com/slimjs/slim.js))

The usual landing page "jumbotron" (in Bootstrap parlance). This component doesn't really use any of the SADE services on the page.

## `<feature-stripe>` ([Tonic](https://tonicframework.dev/))

A "stripe" on the "tiger tail" landing page style that normally contains features or reasons to use the given product/brand.

## `<featurette-card>` ([Tonic](https://tonicframework.dev/))

A card that is normally used in a feature stripe listing common on landing pages. This component makes use of the `<user-service>` which in this case really just collects sentiment, tying certain featurette cards to "intent" or signifying a way of using the product that a user was interested in.

## `<email-collection-stripe>` ([Vue](https://vuejs.org ))

A "stripe" on the "tiger tail" landing page style that normally performs email collection. This compnent makes use of the `<mailing-list-service>` which would register the entered email for some sort of mailing list.

## `<footer>` ([Svelte](https://svelte.dev/docs#Store_contract))

A somewhat normal looking footer, that uses the `<kv-service>` to pull pre-entered data and displays it.
