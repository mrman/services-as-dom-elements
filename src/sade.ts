import { SADEService } from "./types";

export function isSADEElement<T extends SADEService>(obj: any): obj is T {
  return obj && typeof obj === "object" && "supports" in obj && typeof obj.supports === "function";
}

export interface SADEElementGetOpts {
  selector: string,
  requiredURIs: string[],
  document?: Document,
}

export async function getSADEElement<T extends SADEService>(opts: SADEElementGetOpts): Promise<T> {
  const { selector, requiredURIs } = opts;
  if (!selector) { throw new TypeError(`Missing/invalid selector [${selector}]`); }
  if (!requiredURIs) { throw new TypeError(`Missing/invalid list of supported URIs [${requiredURIs}]`); }

  const document = window ? window.document : opts?.document;
  if (!document) { throw new Error("No document object available"); }

  console.log(`[sade] attempting to find SADE service @ [${selector}]...`);
  const svc: any = document.querySelector(selector);

  // Ensure the service supports what we expects
  if (!svc || !isSADEElement(svc)) {
    throw new Error(`[sade] Element @ [${selector}] is not a SADE element`);
  }

  for (const uri of requiredURIs) {
    const supports = await svc.supports(uri);
    if (!supports) {
      throw new Error(`[sade] SADE service @ [${selector}] does not support interface URI [${uri}]`);
    }
  }

  console.log(`[sade] found SADE service @ [${selector}]`);
  return svc as T;
}
