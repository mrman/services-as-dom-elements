import { LitElement, html, css, property, internalProperty, customElement } from "lit-element";
import { classMap } from "lit-html/directives/class-map";
import { WindowScrollService, WindowScrollServiceURI } from "../../types";

// https://github.com/parcel-bundler/parcel/issues/3375
import 'regenerator-runtime/runtime'

@customElement("nav-bar")
export default class NavBarElement extends LitElement {
  // SADE-powered scroll service
  @property({type: String, attribute: "scroll-svc"}) scrollSvc?: string;
  protected _scrollSvc?: WindowScrollService;

  @property({type: String}) brand: string = "Brand";
  @property({type: Boolean, attribute: "reappear-on-scroll-up"}) reappearOnScrollUp: boolean = false;

  protected lastScrollY?: number;

  @internalProperty()
  protected scrollYDelta?: number;

  protected classes: { [key: string]: boolean } = {};

  async connectedCallback() {
    super.connectedCallback();

    // Attempt to find and use the scroll service if one isn't already present
    if (!this._scrollSvc) {
      if (!this.scrollSvc) { throw new Error("[components/nav-bar] Reference to a scroll service required"); }

      console.log(`[components/nav-bar] attempting to find scroll service @ [${this.scrollSvc}]...`);
      const svc: any = document.querySelector(this.scrollSvc);

      // Ensure the service supports what we expects
      if (!svc || typeof svc.supports !== "function") { throw new Error(`[components/nav-bar] Invalid SADE Service @ [${this.scrollSvc}]`); }
      const supported = await svc.supports(WindowScrollServiceURI);

      if (!supported) {
        throw new Error(`[components/nav-bar] SADE service @ [${this.scrollSvc}] does not support interface URI [${WindowScrollServiceURI}]`);
      }

      this._scrollSvc = svc;

      // Register scroll watcher
      await this._scrollSvc?.subscribe(() => {
        if (this.lastScrollY) { this.scrollYDelta = this.lastScrollY - window.scrollY; }
        this.lastScrollY = window.scrollY;
      });
    }
  }

  render() {
    // Reappear on scroll up behavior
    if (this.reappearOnScrollUp) { this.classes["reappear-on-scroll-up"] = true;  }
    if (window.scrollY > this.offsetHeight) {
      if (this.scrollYDelta && this.scrollYDelta > 0) {
        this.classes["scrolling-down"] = false;
      } else {
        this.classes["scrolling-down"] = true;
      }
    }

    return html`
<nav class="${classMap(this.classes)}">

  <span class="lhs">
    <span class="brand">${this.brand}</span>
  </span>

  <span class="rhs">
    <slot></slot>
  </span>

</nav>
`;
  }

  static get styles() {
    return css`
nav {
  width: 100%;

  display: flex;
  justify-content: space-between;
  align-items: center;
  height: 5em;

  background-color: black;
  background: linear-gradient(rgba(10, 10, 10, 0.7), rgba(0,0,0,0.6));
  color: white;
}

nav.reappear-on-scroll-up {
  position: fixed;
  transform: translateY(0);
  z-index: 2;
  animation: show 0.5s ease;
}

@keyframes show {
  from { transform: translateY(-5em); }
  to { transform: translateY(0); }
}

nav.reappear-on-scroll-up.scrolling-down {
 animation: hide 0.5s ease;
 transform: translateY(-5em);
}

@keyframes hide {
  from { transform: translateY(0); }
  to { transform: translateY(-5em); }
}

nav span.lhs {
  margin-left: 1em;
}

nav span.lhs span.brand {
  font-size: 1.5em;
}

nav span.rhs {
  margin-right: 3em;
}

::slotted(a) {
  text-decoration: none;
  color: inherit;
  padding-left: 1em;
  padding-right: 1em;
}

::slotted(a:hover) {
  text-decoration: underline;
}
`;
  }

}
