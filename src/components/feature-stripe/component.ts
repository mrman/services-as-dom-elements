// @ts-nocheck
// Tonic doesn't yet have TS typings

import Tonic from "@optoolco/tonic";

class FeatureStripe extends Tonic {

  renderFeaturetteContainer(featurettes: HTMLElement[] = []): HTMLElement {
    const div = document.createElement(`div`);

    if (!featurettes || featurettes.length === 0) { return div; }

    div.className = "featurette-container";
    featurettes.forEach(f => div.appendChild(f));

    return div;
  }

  render() {
    const featurettes = this.renderFeaturetteContainer(
      // @ts-ignore
      Array.from(this.children)
        .filter(elem => elem.tagName.toLowerCase() === "featurette-card")
    );

    return this.html`
<h1 class="heading">${this.props.heading}</h1>
${featurettes}
`;
  }

  stylesheet() {
    return `
feature-stripe h1.heading {
  margin-top: 2em;
  margin-bottom: 2em;
  text-align: center;
}

feature-stripe div.featurette-container {
  display: flex;
  justify-content: space-between;
  margin-bottom: 5em;
}

feature-stripe div.featurette-container featurette-card {
  flex-basis: 30%;
  margin: 0 1em;;
}

@media (max-width: 768px) {
  feature-stripe div.featurette-container {
    flex-direction: column;
  }

  feature-stripe div.featurette-container featurette-card {
    margin: 2em;
    flex-basis: 80%;
  }
}
`;
  }
}

Tonic.add(FeatureStripe);
