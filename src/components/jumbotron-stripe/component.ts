import { tag, template, useShadow } from "slim-js/Decorators";
import { Slim } from "slim-js";

@tag("jumbotron-stripe")
@template(`
<style>
.component.jumbotron-stripe {
  position: relative;
  margin: 0;
  padding: 10vh 2vw;
  min-height: 50vh;

  font-size: 1.25em;

  background-color: #333;
  color: white;

  position: relative;
  overflow: hidden;

  display: flex;
  flex-direction: column;
  justify-content: center;
}

.component.jumbotron-stripe::before {
  content: "";
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-image: url({{bgImageSrc}});
  background-size: cover;
  filter: blur(2px);
  filter: brightness(50%);
}

.component.jumbotron-stripe .jumbotron-inner {
  position: relative;
  display: flex;
  justify-content: space-between;
  width: 100%;
}

.component.jumbotron-stripe .jumbotron-inner .lhs-container {
  max-width: 50vw;
}

.component.jumbotron-stripe .jumbotron-inner .rhs-container {
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  max-width: 50vw;
}

.component.jumbotron-stripe h1 {
  margin: 0;
  font-weight: 200;
  margin-bottom: 1em;
}

.component.jumbotron-stripe h2 {
  margin: 0;
  font-weight: 300;
  margin-bottom: 0.5em;
}

@media (max-width: 1200px) {
  .component.jumbotron-stripe .jumbotron-inner {
    flex-direction: column;
  }

  .component.jumbotron-stripe .jumbotron-inner .lhs-container {
    max-width: 100vw;
  }

  .component.jumbotron-stripe .jumbotron-inner .rhs-container {
    margin-top: 2em;
    max-width: 100vw;
  }
}

</style>

<section class="component jumbotron-stripe">
  <div class="jumbotron-inner">
    <div class="lhs-container">
      <h1 class="heading-container"><slot name="heading"></slot></h1>
      <h2 class="subheading-container"><slot name="subheading"></slot></h2>
      <div class="cta-container"><slot name="call-to-action"><slot></div>
    </div>
    <div class="rhs-container">
      <slot class="rhs" name="rhs"/>
    </div>
  </div>
</section>
`)
@useShadow(true)
export class JumbotronStripeElement extends Slim {
  protected bgImageSrc?: string;

  static get observedAttributes() { return [ "bg-image-src" ]; }
  get autoBoundAttributes() { return [ "bg-image-src" ]; }

  onCreated() {
    console.log(`[components/jumbotron-stripe] created, bgImageSrc is [${this.bgImageSrc}]`);
  }
}
