// @ts-nocheck
// Tonic doesn't yet have TS typings

import Tonic from "@optoolco/tonic";

import placeholderImageSrc from "./manypixels-finance-app.svg";

import { UserService, UserServiceURI } from "../../types";
import { KeyValueService, KeyValueServiceURI } from "../../types";
import { getSADEElement } from "../../sade";

class FeaturetteCard extends Tonic {
  protected _userSvc?: UserService;
  protected _kvSvc?: UserService;

  async connected() {
    // Look up the SADE user service
    this._userSvc = await getSADEElement({
      selector: this.props.userSvc,
      requiredURIs: [ UserServiceURI ],
    });

    // Look up the SADE KV service
    this._kvSvc = await getSADEElement({
      selector: this.props.kvSvc,
      requiredURIs: [ KeyValueServiceURI ],
    });

  }

  click(e) {
    if (!e.target.matches("button.cta")) { return; }

    this._userSvc.addNewUser(this._kvSvc.get("email"), this.props.ctaInterest);
  }

  render() {
    const heading = Array.from(this.children).find(elem => elem.tagName.toLowerCase() === "heading")?.childNodes;
    const blurb = Array.from(this.children).find(elem => elem.tagName.toLowerCase() === "blurb")?.childNodes;

    const imgSrc = this.props.imgSrc || placeholderImageSrc;
    const ctaText = this.props.ctaText || placeholderImageSrc;

    return this.html`
<div class="content">
  <div class="image-container">
    <img class="heading" src="${imgSrc}"></img>
  </div>
  <h1 class="heading">${heading}</h1>
  <p class="blurb">${blurb}</p>
</div>
<button class="cta">${ctaText}</button>
`;
  }

  stylesheet() {
    return `
featurette-card {
  display: flex;
  flex-direction: column;
  justify-content: flex-start;

  margin: 0;
  border: 1px solid #CCC;
  border-radius: .25em;
  box-shadow: 2px 6px 6px #D6D6D6;

  min-height: 30vh;
}

featurette-card .content {
  flex-grow: 1;
}

featurette-card .content .image-container {
  display: inline-block;
  text-align: center;
  width: 100%;
}

featurette-card .content .image-container img {
  max-width: 50%;
}

featurette-card h1.heading {
  margin: 0;
  margin-top: .5em;
  margin-bottom: .5em;
}

featurette-card p.blurb {
  margin: 0 1em 1em 1em;
}

featurette-card button.cta {
  max-width: 100%;
  margin: 0 1em;
  padding: 1em;
  box-sizing: border-box;

  cursor: pointer;
  background: linear-gradient(#283e51, #1d2e3c);
  font-size: 1.25em;
  color: white;
  border-radius: 0.25em;
  border: 0;
  margin-bottom: 1em;

}
`;
  }
}

Tonic.add(FeaturetteCard);
