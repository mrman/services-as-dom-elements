import Vue from "vue";
import wrap from "@vue/web-component-wrapper";
import EmailCollectionStripe from "./component.vue";

window.customElements.define(
  "email-collection-stripe",
  // @ts-ignore
  wrap(Vue, EmailCollectionStripe),
);
