import { SADEService, KeyValueServiceURI } from "../../types";
import { KeyValueService } from "./service";

/**
 * Wrapper element that exposes the KeyValueService to other elements
 */
export class KeyValueServiceElement extends SADEService {
  protected kvSvc;
  protected uris: string[] = [
    KeyValueServiceURI,
  ]

  protected observer?: MutationObserver;

  constructor() {
    super();
    this.kvSvc = new KeyValueService();
  }

  connectedCallback() {
    // Set up a lsitener for the child update so that we can deal with elements in body
    this.observer = new MutationObserver(() => this.onChildUpdate());
    this.observer.observe(this, { childList: true });
  }

  onChildUpdate() {
    const preloadElems = this.querySelectorAll("initial-data");
    preloadElems.forEach(dataPreloadElement => {
      dataPreloadElement
        .querySelectorAll('datum')
        .forEach(datumElement => this.preloadDatumElement(datumElement));
    });
  }

  preloadDatumElement(elem: Element) {
    const key = elem.getAttribute("key");
    if (!key) {
      console.warn("[kv-service-component] <preload> element is missing 'key' attribute:", elem);
      return;
    }

    const format = elem.getAttribute("format");
    if (!format) {
      console.warn("[kv-service-component] <preload> element is missing 'format' attribute:", elem);
      return;
    }

    if (format && format === "json") { this.preloadJSONElement(key, elem); }
  }

  preloadJSONElement(key: string, elem: Element) {
    if (!elem.textContent) { return; }
    try {
      this.kvSvc.set(key, JSON.parse(elem.textContent));
      console.log(`[kv-service-component] successfully preloaded key [${key}]`);
    } catch (err) {
      console.log("[kv-service-component] Invalid JSON failed to perform preload:", err);
    }
  }

  get(key: string): any {
    if (!this.kvSvc) { throw new Error("Unexpected error: this.kvSvc not available"); }
    return this.kvSvc.get(key);
  }

  set(key: string, value: string): any {
    if (!this.kvSvc) { throw new Error("Unexpected error: this.kvSvc not available"); }
    return this.kvSvc.set(key, value);
  }
}

// Register the custom element
customElements.define("key-value-service", KeyValueServiceElement);
