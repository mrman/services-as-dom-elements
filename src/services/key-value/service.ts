/**
 * The KeyValueService performs functions that a deal with managing users
 *
 */
export class KeyValueService implements KeyValueService {

  protected data: { [key: string]: any } = {};

  constructor() {
    console.log("[kv-svc] constructed");
  }

  // @see KeyValueService
  get(key: string): any {
    const value = this.data[key];
    if (!value) { return key; }
    return value;
  }

  // @see KeyValueService
  set(key: string, value: string): any {
    return this.data[key] = value;
  }

}
