import { SADEService, WindowScrollServiceURI } from "../../types";
import { BrowserWindowScrollService } from "./service";

// https://github.com/parcel-bundler/parcel/issues/3375
import 'regenerator-runtime/runtime'

/**
 * Wrapper element that exposes the WindowScrollService to other elements
 *
 * This could use different methods of scroll watching, but now only does it via the browser
 */
export class WindowScrollServiceElement extends SADEService {
  protected browserScrollSvc: BrowserWindowScrollService;
  protected uris: string[] = [ WindowScrollServiceURI ];

  constructor() {
    super();
    this.browserScrollSvc = new BrowserWindowScrollService();

    console.log("[scroll-svc] constructed");
  }

  async subscribe(handlerFn: EventListener): Promise<number> {
    if (!this.browserScrollSvc) { throw new Error("Unexpected error: this.browserScrollSvc not available"); }
    // TODO: more fancy functionality  at the DOM level?
    return this.browserScrollSvc.subscribe(handlerFn);
  }

  async unsubscribe(id: number): Promise<void> {
    if (!this.browserScrollSvc) { throw new Error("Unexpected error: this.browserScrollSvc not available"); }
    // TODO: more fancy functionality  at the DOM level?
    return this.browserScrollSvc.unsubscribe(id);
  }
}

// Register the custom element
customElements.define("window-scroll-service", WindowScrollServiceElement);
