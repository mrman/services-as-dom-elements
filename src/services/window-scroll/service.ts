import { WindowScrollService } from "../../types";

/**
 * The WindowScrollService watches and reports window scrolling activity
 *
 */
export class BrowserWindowScrollService implements WindowScrollService {

  protected handlerId = 0;
  protected handlers: {[key: number]: EventListener} = {};

  async subscribe(handlerFn: EventListener): Promise<number> {
    // Get an ID to represent the handler
    const id = ++this.handlerId;
    this.handlers[id] = handlerFn;

    // Add the event listener
    window.addEventListener("scroll", handlerFn);

    return id;
  }

  async unsubscribe(id: number) {
    const handlerFn = this.handlers[id];
    if (!handlerFn) { throw new Error("No such handler, invalid symbol reference"); }

    window.removeEventListener("scroll", handlerFn);

    delete this.handlers[id];
  }

}
