import { SADEService, MailingListServiceURI } from "../../types";
import { MailingListService } from "./service";

/**
 * Wrapper element that exposes the MailingListService to other elements
 */
export class MailingListServiceElement extends SADEService {
  protected mailingListSvc: MailingListService;
  protected uris: string[] = [
    MailingListServiceURI,
  ]

  constructor() {
    super();

    this.mailingListSvc = new MailingListService({
      baseURL: this.getAttribute("base-url") ?? undefined,
      addSubscriberPath: this.getAttribute("add-subscriber-path") ?? undefined,
    });
  }

  async addNewSubscriber(mailingListID: string, email: string, segments?: string[]): Promise<void> {
    if (!this.mailingListSvc) { throw new Error("Unexpected error: this.mailingListSvc not available"); }

    try {
      await this.mailingListSvc.addNewSubscriber(mailingListID, email, segments);
    } catch (err) {
      alert(`[mailing-list-service]\n\nPOST failed when add new subscriber with email: ${email}\n\nERROR: ${err.message}`);
    }
  }
}

// Register the custom element
customElements.define("mailing-list-service", MailingListServiceElement);
