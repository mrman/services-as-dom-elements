import axios, { AxiosInstance } from "axios";
import isEmail from "validator/lib/isEmail";

const DEFAULT_BASE_URL = "/api"
const DEFAULT_NEW_USER_PATH = "v1/mailing-lists/{mailingListID}/subscribers"

export interface MailingListServiceOpts {
  baseURL?: string;
  addSubscriberPath?: string;
}

/**
 * The MailingListService performs functions that a deal with managing users
 *
 */
export class MailingListService implements MailingListService {

  protected baseURL: string;
  protected addSubscriberPath: string;
  protected axios: AxiosInstance;

  constructor(opts: MailingListServiceOpts) {
    this.baseURL = opts?.baseURL ?? DEFAULT_BASE_URL;
    if (typeof this.baseURL !== "string") { throw new TypeError(`Invalid base URL [${this.baseURL}]`); }

    this.addSubscriberPath = opts?.addSubscriberPath ?? DEFAULT_NEW_USER_PATH;
    if (typeof this.addSubscriberPath !== "string") {
      throw new TypeError(`Invalid new user path [${this.addSubscriberPath}]`);
    }

    this.axios = axios.create({
      baseURL: this.baseURL,
    });

    console.log("[mailing-list-svc] constructed");
  }

  protected generateNewSubscriberURL(mailingListID: string): string {
    const mailingListPath = this.addSubscriberPath.replace("{mailingListID}", mailingListID);
    return `${this.baseURL}/${mailingListPath}`;
  }

  // @see MailingListService
  async addNewSubscriber(mailingListID: string, email: string, segments: string[] = []): Promise<void> {
    if (!isEmail(email)) { throw new TypeError(`Email [${email}] is not a valid email address`); }
    if (typeof mailingListID !== "string") { throw new TypeError("Invalid Mailing List ID"); }
    if (!Array.isArray(segments)) { throw new TypeError("Invalid segments array"); }
    if (segments.length > 0 && segments.every(v => typeof v === "string")) { throw new TypeError("Segment array contents must be strings"); }

    await this.axios.post(this.generateNewSubscriberURL(mailingListID), { email, segments });
    console.log(`[mailing-list-svc] subscriber [${email}] added to mailing list [${mailingListID}] (segments: ${segments.join(",")})`);
  }

}
