import { SADEService, UserServiceURI } from "../../types";
import { UserService } from "./service";

/**
 * Wrapper element that exposes the UserService to other elements
 */
export class UserServiceElement extends SADEService {
  protected userSvc;
  protected uris: string[] = [
    UserServiceURI,
  ]

  constructor() {
    super();

    this.userSvc = new UserService({
      baseURL: this.getAttribute("base-url") ?? undefined,
      newUserPath: this.getAttribute("new-user-path") ?? undefined,
    });
  }

  async addNewUser(email: string, interest: string): Promise<void> {
    if (!this.userSvc) { throw new Error("Unexpected error: this.userSvc not available"); }

    try {
      await this.userSvc.addNewUser(email, interest);
    } catch (err) {
      alert(`[user-service]\n\nPOST failed when add new user who was interested in: [${interest}]\n\nERROR: ${err.message}`);
    }
  }
}

// Register the custom element
customElements.define("user-service", UserServiceElement);
