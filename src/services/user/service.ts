import axios, { AxiosInstance } from "axios";

const DEFAULT_BASE_URL = "/api"
const DEFAULT_NEW_USER_PATH = "v1/users"

export interface UserServiceOpts {
  baseURL?: string;
  newUserPath?: string;
}

/**
 * The UserService performs functions that a deal with managing users
 *
 */
export class UserService implements UserService {

  protected baseURL: string;
  protected newUserPath: string;
  protected axios: AxiosInstance;

  constructor(opts: UserServiceOpts) {
    this.baseURL = opts?.baseURL ?? DEFAULT_BASE_URL;
    if (typeof this.baseURL !== "string") { throw new TypeError(`Invalid base URL [${this.baseURL}]`); }

    this.newUserPath = opts?.newUserPath ?? DEFAULT_NEW_USER_PATH;
    if (typeof this.newUserPath !== "string") { throw new TypeError(`Invalid new user path [${this.newUserPath}]`); }

    this.axios = axios.create({
      baseURL: this.baseURL,
    });

    console.log("[user-svc] constructed");
  }

  protected generateNewUserURL() { return `${this.baseURL}/${this.newUserPath}`; }

  // @see UserService
  async addNewUser(email: string, interest: string): Promise<void> {
    await this.axios.post(this.generateNewUserURL(), { email, interest });
    console.log(`[user-svc] new user with email [${email}] added`);
  }

}
