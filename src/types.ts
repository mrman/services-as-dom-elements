/**
 * SADE-compliant services need only list what capabilities they support. Clients must be able to
 * determine what functionality is expected to be available as a result.
 */
export abstract class SADEService extends HTMLElement {
  protected uris: string[] = [];

  // Check whether the SADEService adheres to a given API, expressed as a URI
  async supports(uri: string): Promise<boolean> {
    return this.uris.includes(uri);
  }
}

/**
 * A generic user service for doing things often done on landing pages
 *
 */
export interface UserService {
  /**
   * Add a new user by email
   *
   * @param {string} email
   */
  addNewUser(email: string): Promise<void>;
}

export const UserServiceURI = "urn:sade-example:user-service/v1";

/**
 * A generic user service for doing things often done on landing pages
 *
 */
export interface MailingListService {
  /**
   * Add a new subscriber by email
   *
   * @param {string} mailingListID
   * @param {string} email
   * @param {string[]} [segments] - mailing list segments similar to tags (ex. "enterprise")
   */
  addNewSubscriber(mailingListID: string, email: string, segments?: string[]): Promise<void>;
}

export const MailingListServiceURI = "urn:sade-example:mailing-list-service/v1";

/**
 * A generic service for watching/reacting to scroll behavior on the window
 *
 */
export interface WindowScrollService {
  /**
   * Subscribe to scroll updates for the page
   *
   * @param {function} handlerFn that will be called on the page
   * @return {Promise<string>} An ID for the handler
   */
  subscribe(handlerFn: EventListener): Promise<number>;

  /**
   * Unsubscribe a given scroll watcher by symbol
   *
   * @param {symbol} sym
   * @return {Promise<void>}
   */
  unsubscribe(id: number): Promise<void>;
}

export const WindowScrollServiceURI = "urn:sade-example:window-scroll-service/v1";

/**
 * A generic key value service for getting information stored via key/value
 *
 */
export interface KeyValueService {
  get(key: string): any;
  set(key: string, value: any): any;
}

export const KeyValueServiceURI = "urn:sade-example:key-value-service/v1";
