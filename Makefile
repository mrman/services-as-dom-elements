.PHONY: all install lint \
				build build-ts build-ts-watch build-gitlab-pages \
				serve

all: install build

YARN ?= yarn

install:
	$(YARN) install

lint:
	$(YARN) lint

build:
	$(YARN) build

build-ts:
	$(YARN) build-ts

build-ts-watch:
	$(YARN) build-ts-watch

serve:
	$(YARN) serve

# Expecting the public URL for the gitlab pages to be https://<user>.gitlab.io/services-as-dom-elements/...
build-gitlab-pages:
	$(YARN) build-demo -- --public-url "/services-as-dom-elements" -d public
